Hangman Web Client
========================
This web client needs [HangmanWebService](https://github.com/okdios/HangmanWebService) in order to run.

![ScreenShot](http://okdios.dk/images/hangman_web_screenshot.png)
