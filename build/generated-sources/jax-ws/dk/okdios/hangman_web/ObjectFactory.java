
package dk.okdios.hangman_web;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the dk.okdios.hangman_web package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetRemainingAttemptsAsString_QNAME = new QName("http://hangman_web.okdios.dk/", "getRemainingAttemptsAsString");
    private final static QName _RestartGame_QNAME = new QName("http://hangman_web.okdios.dk/", "restartGame");
    private final static QName _TestGetWord_QNAME = new QName("http://hangman_web.okdios.dk/", "test_getWord");
    private final static QName _GetUsedLettersAsStringResponse_QNAME = new QName("http://hangman_web.okdios.dk/", "getUsedLettersAsStringResponse");
    private final static QName _GetRemainingAttemptsAsStringResponse_QNAME = new QName("http://hangman_web.okdios.dk/", "getRemainingAttemptsAsStringResponse");
    private final static QName _Won_QNAME = new QName("http://hangman_web.okdios.dk/", "won");
    private final static QName _CheckLetter_QNAME = new QName("http://hangman_web.okdios.dk/", "checkLetter");
    private final static QName _GetRemainingAttempts_QNAME = new QName("http://hangman_web.okdios.dk/", "getRemainingAttempts");
    private final static QName _GetFoundAsStringResponse_QNAME = new QName("http://hangman_web.okdios.dk/", "getFoundAsStringResponse");
    private final static QName _GetFoundAsString_QNAME = new QName("http://hangman_web.okdios.dk/", "getFoundAsString");
    private final static QName _IsGameOver_QNAME = new QName("http://hangman_web.okdios.dk/", "isGameOver");
    private final static QName _CheckLetterResponse_QNAME = new QName("http://hangman_web.okdios.dk/", "checkLetterResponse");
    private final static QName _RestartGameResponse_QNAME = new QName("http://hangman_web.okdios.dk/", "restartGameResponse");
    private final static QName _GetRemainingAttemptsResponse_QNAME = new QName("http://hangman_web.okdios.dk/", "getRemainingAttemptsResponse");
    private final static QName _GetUsedLettersAsString_QNAME = new QName("http://hangman_web.okdios.dk/", "getUsedLettersAsString");
    private final static QName _WonResponse_QNAME = new QName("http://hangman_web.okdios.dk/", "wonResponse");
    private final static QName _GetWordAsString_QNAME = new QName("http://hangman_web.okdios.dk/", "getWordAsString");
    private final static QName _TestGetWordResponse_QNAME = new QName("http://hangman_web.okdios.dk/", "test_getWordResponse");
    private final static QName _IsGameOverResponse_QNAME = new QName("http://hangman_web.okdios.dk/", "isGameOverResponse");
    private final static QName _GetWordAsStringResponse_QNAME = new QName("http://hangman_web.okdios.dk/", "getWordAsStringResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: dk.okdios.hangman_web
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetFoundAsString }
     * 
     */
    public GetFoundAsString createGetFoundAsString() {
        return new GetFoundAsString();
    }

    /**
     * Create an instance of {@link IsGameOver }
     * 
     */
    public IsGameOver createIsGameOver() {
        return new IsGameOver();
    }

    /**
     * Create an instance of {@link CheckLetterResponse }
     * 
     */
    public CheckLetterResponse createCheckLetterResponse() {
        return new CheckLetterResponse();
    }

    /**
     * Create an instance of {@link RestartGameResponse }
     * 
     */
    public RestartGameResponse createRestartGameResponse() {
        return new RestartGameResponse();
    }

    /**
     * Create an instance of {@link GetRemainingAttemptsResponse }
     * 
     */
    public GetRemainingAttemptsResponse createGetRemainingAttemptsResponse() {
        return new GetRemainingAttemptsResponse();
    }

    /**
     * Create an instance of {@link GetUsedLettersAsString }
     * 
     */
    public GetUsedLettersAsString createGetUsedLettersAsString() {
        return new GetUsedLettersAsString();
    }

    /**
     * Create an instance of {@link WonResponse }
     * 
     */
    public WonResponse createWonResponse() {
        return new WonResponse();
    }

    /**
     * Create an instance of {@link GetWordAsString }
     * 
     */
    public GetWordAsString createGetWordAsString() {
        return new GetWordAsString();
    }

    /**
     * Create an instance of {@link TestGetWordResponse }
     * 
     */
    public TestGetWordResponse createTestGetWordResponse() {
        return new TestGetWordResponse();
    }

    /**
     * Create an instance of {@link IsGameOverResponse }
     * 
     */
    public IsGameOverResponse createIsGameOverResponse() {
        return new IsGameOverResponse();
    }

    /**
     * Create an instance of {@link GetWordAsStringResponse }
     * 
     */
    public GetWordAsStringResponse createGetWordAsStringResponse() {
        return new GetWordAsStringResponse();
    }

    /**
     * Create an instance of {@link GetRemainingAttemptsAsString }
     * 
     */
    public GetRemainingAttemptsAsString createGetRemainingAttemptsAsString() {
        return new GetRemainingAttemptsAsString();
    }

    /**
     * Create an instance of {@link TestGetWord }
     * 
     */
    public TestGetWord createTestGetWord() {
        return new TestGetWord();
    }

    /**
     * Create an instance of {@link RestartGame }
     * 
     */
    public RestartGame createRestartGame() {
        return new RestartGame();
    }

    /**
     * Create an instance of {@link GetUsedLettersAsStringResponse }
     * 
     */
    public GetUsedLettersAsStringResponse createGetUsedLettersAsStringResponse() {
        return new GetUsedLettersAsStringResponse();
    }

    /**
     * Create an instance of {@link Won }
     * 
     */
    public Won createWon() {
        return new Won();
    }

    /**
     * Create an instance of {@link GetRemainingAttemptsAsStringResponse }
     * 
     */
    public GetRemainingAttemptsAsStringResponse createGetRemainingAttemptsAsStringResponse() {
        return new GetRemainingAttemptsAsStringResponse();
    }

    /**
     * Create an instance of {@link CheckLetter }
     * 
     */
    public CheckLetter createCheckLetter() {
        return new CheckLetter();
    }

    /**
     * Create an instance of {@link GetFoundAsStringResponse }
     * 
     */
    public GetFoundAsStringResponse createGetFoundAsStringResponse() {
        return new GetFoundAsStringResponse();
    }

    /**
     * Create an instance of {@link GetRemainingAttempts }
     * 
     */
    public GetRemainingAttempts createGetRemainingAttempts() {
        return new GetRemainingAttempts();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRemainingAttemptsAsString }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "getRemainingAttemptsAsString")
    public JAXBElement<GetRemainingAttemptsAsString> createGetRemainingAttemptsAsString(GetRemainingAttemptsAsString value) {
        return new JAXBElement<GetRemainingAttemptsAsString>(_GetRemainingAttemptsAsString_QNAME, GetRemainingAttemptsAsString.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RestartGame }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "restartGame")
    public JAXBElement<RestartGame> createRestartGame(RestartGame value) {
        return new JAXBElement<RestartGame>(_RestartGame_QNAME, RestartGame.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestGetWord }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "test_getWord")
    public JAXBElement<TestGetWord> createTestGetWord(TestGetWord value) {
        return new JAXBElement<TestGetWord>(_TestGetWord_QNAME, TestGetWord.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsedLettersAsStringResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "getUsedLettersAsStringResponse")
    public JAXBElement<GetUsedLettersAsStringResponse> createGetUsedLettersAsStringResponse(GetUsedLettersAsStringResponse value) {
        return new JAXBElement<GetUsedLettersAsStringResponse>(_GetUsedLettersAsStringResponse_QNAME, GetUsedLettersAsStringResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRemainingAttemptsAsStringResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "getRemainingAttemptsAsStringResponse")
    public JAXBElement<GetRemainingAttemptsAsStringResponse> createGetRemainingAttemptsAsStringResponse(GetRemainingAttemptsAsStringResponse value) {
        return new JAXBElement<GetRemainingAttemptsAsStringResponse>(_GetRemainingAttemptsAsStringResponse_QNAME, GetRemainingAttemptsAsStringResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Won }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "won")
    public JAXBElement<Won> createWon(Won value) {
        return new JAXBElement<Won>(_Won_QNAME, Won.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckLetter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "checkLetter")
    public JAXBElement<CheckLetter> createCheckLetter(CheckLetter value) {
        return new JAXBElement<CheckLetter>(_CheckLetter_QNAME, CheckLetter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRemainingAttempts }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "getRemainingAttempts")
    public JAXBElement<GetRemainingAttempts> createGetRemainingAttempts(GetRemainingAttempts value) {
        return new JAXBElement<GetRemainingAttempts>(_GetRemainingAttempts_QNAME, GetRemainingAttempts.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFoundAsStringResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "getFoundAsStringResponse")
    public JAXBElement<GetFoundAsStringResponse> createGetFoundAsStringResponse(GetFoundAsStringResponse value) {
        return new JAXBElement<GetFoundAsStringResponse>(_GetFoundAsStringResponse_QNAME, GetFoundAsStringResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFoundAsString }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "getFoundAsString")
    public JAXBElement<GetFoundAsString> createGetFoundAsString(GetFoundAsString value) {
        return new JAXBElement<GetFoundAsString>(_GetFoundAsString_QNAME, GetFoundAsString.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsGameOver }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "isGameOver")
    public JAXBElement<IsGameOver> createIsGameOver(IsGameOver value) {
        return new JAXBElement<IsGameOver>(_IsGameOver_QNAME, IsGameOver.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckLetterResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "checkLetterResponse")
    public JAXBElement<CheckLetterResponse> createCheckLetterResponse(CheckLetterResponse value) {
        return new JAXBElement<CheckLetterResponse>(_CheckLetterResponse_QNAME, CheckLetterResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RestartGameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "restartGameResponse")
    public JAXBElement<RestartGameResponse> createRestartGameResponse(RestartGameResponse value) {
        return new JAXBElement<RestartGameResponse>(_RestartGameResponse_QNAME, RestartGameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRemainingAttemptsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "getRemainingAttemptsResponse")
    public JAXBElement<GetRemainingAttemptsResponse> createGetRemainingAttemptsResponse(GetRemainingAttemptsResponse value) {
        return new JAXBElement<GetRemainingAttemptsResponse>(_GetRemainingAttemptsResponse_QNAME, GetRemainingAttemptsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsedLettersAsString }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "getUsedLettersAsString")
    public JAXBElement<GetUsedLettersAsString> createGetUsedLettersAsString(GetUsedLettersAsString value) {
        return new JAXBElement<GetUsedLettersAsString>(_GetUsedLettersAsString_QNAME, GetUsedLettersAsString.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "wonResponse")
    public JAXBElement<WonResponse> createWonResponse(WonResponse value) {
        return new JAXBElement<WonResponse>(_WonResponse_QNAME, WonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWordAsString }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "getWordAsString")
    public JAXBElement<GetWordAsString> createGetWordAsString(GetWordAsString value) {
        return new JAXBElement<GetWordAsString>(_GetWordAsString_QNAME, GetWordAsString.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestGetWordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "test_getWordResponse")
    public JAXBElement<TestGetWordResponse> createTestGetWordResponse(TestGetWordResponse value) {
        return new JAXBElement<TestGetWordResponse>(_TestGetWordResponse_QNAME, TestGetWordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsGameOverResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "isGameOverResponse")
    public JAXBElement<IsGameOverResponse> createIsGameOverResponse(IsGameOverResponse value) {
        return new JAXBElement<IsGameOverResponse>(_IsGameOverResponse_QNAME, IsGameOverResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWordAsStringResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hangman_web.okdios.dk/", name = "getWordAsStringResponse")
    public JAXBElement<GetWordAsStringResponse> createGetWordAsStringResponse(GetWordAsStringResponse value) {
        return new JAXBElement<GetWordAsStringResponse>(_GetWordAsStringResponse_QNAME, GetWordAsStringResponse.class, null, value);
    }

}
