package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.json.simple.JSONObject;

public final class actions_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {


void sendResult(String result)
{
    
}

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");

String responseName = "result";
String action = request.getParameter("action");
String arg = request.getParameter("arg");
if (arg == null)
    arg = "";
//String debug = request.getParameter("DEBUG");

//out.println("<hr>Action: >" + action + "<<hr>");
if (action != null)
{
    if (action.equals("test"))
    {
        JSONObject obj=new JSONObject();
        obj.put("result","foo");
        out.print(obj);
        out.flush();
    }
    /*************************************
     * TEST: get a new random word
     */
    else if (action.equals("test_getWord"))
    {
        try {
            dk.okdios.hangman_web.WebApplicationWS1 service = new dk.okdios.hangman_web.WebApplicationWS1();
            dk.okdios.hangman_web.HangmanWEB port = service.getHangmanWEBPort();
            // TODO process result here
            java.lang.String result = port.testGetWord();
            //out.println("Result = "+result);
            JSONObject obj=new JSONObject();
            obj.put(responseName,result);
            out.print(obj);
            out.flush();
        } catch (Exception ex) {
    	// TODO handle custom exceptions here
            //out.println("exception: " + ex);
        }
    }
    /*************************************
     * Get word with only found letters "visible"
     */
    else if (action.equals("getFoundAsString"))
    {
        try {
            dk.okdios.hangman_web.WebApplicationWS1 service = new dk.okdios.hangman_web.WebApplicationWS1();
            dk.okdios.hangman_web.HangmanWEB port = service.getHangmanWEBPort();
            // TODO process result here
            java.lang.String result = port.getFoundAsString();
            //out.println("Result = "+result);
            JSONObject obj=new JSONObject();
            obj.put(responseName,result);
            out.print(obj);
            out.flush();
        } catch (Exception ex) {
    	// TODO handle custom exceptions here
            //out.println("exception: " + ex);
        }
    }
    /*************************************
     * Get already used letters (which are not in the word)
     */
    else if (action.equals("getUsedLettersAsString"))
    {
        try {
            dk.okdios.hangman_web.WebApplicationWS1 service = new dk.okdios.hangman_web.WebApplicationWS1();
            dk.okdios.hangman_web.HangmanWEB port = service.getHangmanWEBPort();
            // TODO process result here
            java.lang.String result = port.getUsedLettersAsString();
            //out.println("Result = "+result);
            JSONObject obj=new JSONObject();
            obj.put(responseName,result);
            out.print(obj);
            out.flush();
        } catch (Exception ex) {
    	// TODO handle custom exceptions here
            //out.println("exception: " + ex);
        }
    }
    /*************************************
     * Get already used letters (that are not in the word)
     */
    else if (action.equals("getRemainingAttemptsAsString"))
    {
        try {
            dk.okdios.hangman_web.WebApplicationWS1 service = new dk.okdios.hangman_web.WebApplicationWS1();
            dk.okdios.hangman_web.HangmanWEB port = service.getHangmanWEBPort();
            // TODO process result here
            java.lang.String result = port.getRemainingAttemptsAsString();
            //out.println("Result = "+result);
            JSONObject obj=new JSONObject();
            obj.put(responseName,result);
            out.print(obj);
            out.flush();
        } catch (Exception ex) {
    	// TODO handle custom exceptions here
            //out.println("exception: " + ex);
        }
    }
    /*************************************
     * Get already used letters (which are not in the word)
     */
    else if (action.equals("checkLetter"))
    {
        try {
            dk.okdios.hangman_web.WebApplicationWS1 service = new dk.okdios.hangman_web.WebApplicationWS1();
            dk.okdios.hangman_web.HangmanWEB port = service.getHangmanWEBPort();
            // TODO process result here
            java.lang.Boolean result = port.checkLetter(arg);
            //out.println("Result = "+result);
            JSONObject obj=new JSONObject();
            obj.put(responseName,result);
            out.print(obj);
            out.flush();
        } catch (Exception ex) {
    	// TODO handle custom exceptions here
            //out.println("exception: " + ex);
        }
    }
    /*************************************
     * Restart game
     */
    else if (action.equals("restartGame"))
    {
        try {
            dk.okdios.hangman_web.WebApplicationWS1 service = new dk.okdios.hangman_web.WebApplicationWS1();
            dk.okdios.hangman_web.HangmanWEB port = service.getHangmanWEBPort();
            // TODO process result here
            port.restartGame();
            //out.println("Result = "+result);
            JSONObject obj=new JSONObject();
            obj.put(responseName,"restarted");
            out.print(obj);
            out.flush();
        } catch (Exception ex) {
    	// TODO handle custom exceptions here
            //out.println("exception: " + ex);
        }
    }
}
else
{
    JSONObject obj=new JSONObject();
    obj.put("error","no action");
    out.print(obj);
    out.flush();
}


      out.write('\n');
      out.write('\n');
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
