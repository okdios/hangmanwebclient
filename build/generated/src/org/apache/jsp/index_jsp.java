package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Hangman</title>\n");
      out.write("        <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js\"></script>\n");
      out.write("        <script src=\"res/js/javascript.js\"></script>\n");
      out.write("        <link rel=\"stylesheet\" href=\"res/css/style.css\" type=\"text/css\" />\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        \n");
      out.write("        <!-- Wrapper start -->\n");
      out.write("        <div id=\"wrapper\">\n");
      out.write("        \n");
      out.write("        <!-- Header -->    \n");
      out.write("        <h1>Hangman</h1>\n");
      out.write("        \n");
      out.write("        <!-- Canvas for hangman drawing -->\n");
      out.write("        <canvas id=\"hangmanCanvas\" width=\"600\" height=\"300\"></canvas>\n");
      out.write("        <script src=\"res/js/canvas.js\"></script>\n");
      out.write("        <br>\n");
      out.write("        \n");
      out.write("        <!-- Display found and used letters -->\n");
      out.write("        <span id=\"word_found\">word...</span><br>\n");
      out.write("        <span id=\"used_letters\">used...</span><br>\n");
      out.write("        <span id=\"remaining_attempts\">attempts</span><br>\n");
      out.write("        <br>\n");
      out.write("        \n");
      out.write("        <!-- Display all the letters -->\n");
      out.write("        <div id=\"letterWrapper\">\n");
      out.write("        ");
 // print all the letters
        char[] letters = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        for ( char c : letters)
        {
            
      out.write("<span class=\"letters\">");
out.print(c);
      out.write(" </span>");

        }
        
      out.write("\n");
      out.write("        </div>\n");
      out.write("        <br>\n");
      out.write("        \n");
      out.write("        <!-- Restart button -->\n");
      out.write("        <input type=\"button\" id=\"buttonRestartGame\" value=\"Restart game\"></input>\n");
      out.write("        \n");
      out.write("        <!-- Wrapper end -->\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
