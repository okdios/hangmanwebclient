
package dk.okdios.hangman_web;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebService(name = "HangmanWEB", targetNamespace = "http://hangman_web.okdios.dk/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface HangmanWEB {


    /**
     * 
     * @return
     *     returns boolean
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "isGameOver", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.IsGameOver")
    @ResponseWrapper(localName = "isGameOverResponse", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.IsGameOverResponse")
    @Action(input = "http://hangman_web.okdios.dk/HangmanWEB/isGameOverRequest", output = "http://hangman_web.okdios.dk/HangmanWEB/isGameOverResponse")
    public boolean isGameOver();

    /**
     * 
     * @return
     *     returns boolean
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "won", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.Won")
    @ResponseWrapper(localName = "wonResponse", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.WonResponse")
    @Action(input = "http://hangman_web.okdios.dk/HangmanWEB/wonRequest", output = "http://hangman_web.okdios.dk/HangmanWEB/wonResponse")
    public boolean won();

    /**
     * 
     * @return
     *     returns int
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getRemainingAttempts", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.GetRemainingAttempts")
    @ResponseWrapper(localName = "getRemainingAttemptsResponse", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.GetRemainingAttemptsResponse")
    @Action(input = "http://hangman_web.okdios.dk/HangmanWEB/getRemainingAttemptsRequest", output = "http://hangman_web.okdios.dk/HangmanWEB/getRemainingAttemptsResponse")
    public int getRemainingAttempts();

    /**
     * 
     * @return
     *     returns java.lang.String
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getRemainingAttemptsAsString", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.GetRemainingAttemptsAsString")
    @ResponseWrapper(localName = "getRemainingAttemptsAsStringResponse", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.GetRemainingAttemptsAsStringResponse")
    @Action(input = "http://hangman_web.okdios.dk/HangmanWEB/getRemainingAttemptsAsStringRequest", output = "http://hangman_web.okdios.dk/HangmanWEB/getRemainingAttemptsAsStringResponse")
    public String getRemainingAttemptsAsString();

    /**
     * 
     * @param letter
     * @return
     *     returns boolean
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "checkLetter", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.CheckLetter")
    @ResponseWrapper(localName = "checkLetterResponse", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.CheckLetterResponse")
    @Action(input = "http://hangman_web.okdios.dk/HangmanWEB/checkLetterRequest", output = "http://hangman_web.okdios.dk/HangmanWEB/checkLetterResponse")
    public boolean checkLetter(
        @WebParam(name = "letter", targetNamespace = "")
        String letter);

    /**
     * 
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "test_getWord")
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "test_getWord", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.TestGetWord")
    @ResponseWrapper(localName = "test_getWordResponse", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.TestGetWordResponse")
    @Action(input = "http://hangman_web.okdios.dk/HangmanWEB/test_getWordRequest", output = "http://hangman_web.okdios.dk/HangmanWEB/test_getWordResponse")
    public String testGetWord();

    /**
     * 
     * @return
     *     returns java.lang.String
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getUsedLettersAsString", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.GetUsedLettersAsString")
    @ResponseWrapper(localName = "getUsedLettersAsStringResponse", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.GetUsedLettersAsStringResponse")
    @Action(input = "http://hangman_web.okdios.dk/HangmanWEB/getUsedLettersAsStringRequest", output = "http://hangman_web.okdios.dk/HangmanWEB/getUsedLettersAsStringResponse")
    public String getUsedLettersAsString();

    /**
     * 
     * @return
     *     returns java.lang.String
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getWordAsString", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.GetWordAsString")
    @ResponseWrapper(localName = "getWordAsStringResponse", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.GetWordAsStringResponse")
    @Action(input = "http://hangman_web.okdios.dk/HangmanWEB/getWordAsStringRequest", output = "http://hangman_web.okdios.dk/HangmanWEB/getWordAsStringResponse")
    public String getWordAsString();

    /**
     * 
     * @return
     *     returns java.lang.String
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getFoundAsString", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.GetFoundAsString")
    @ResponseWrapper(localName = "getFoundAsStringResponse", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.GetFoundAsStringResponse")
    @Action(input = "http://hangman_web.okdios.dk/HangmanWEB/getFoundAsStringRequest", output = "http://hangman_web.okdios.dk/HangmanWEB/getFoundAsStringResponse")
    public String getFoundAsString();

    /**
     * 
     */
    @WebMethod
    @RequestWrapper(localName = "restartGame", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.RestartGame")
    @ResponseWrapper(localName = "restartGameResponse", targetNamespace = "http://hangman_web.okdios.dk/", className = "dk.okdios.hangman_web.RestartGameResponse")
    @Action(input = "http://hangman_web.okdios.dk/HangmanWEB/restartGameRequest", output = "http://hangman_web.okdios.dk/HangmanWEB/restartGameResponse")
    public void restartGame();

}
