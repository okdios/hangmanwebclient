<%-- 
    Document   : index
    Created on : Nov 7, 2013, 10:23:47 AM
    Author     : okd
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hangman</title>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="res/js/javascript.js"></script>
        <link rel="stylesheet" href="res/css/style.css" type="text/css" />
    </head>
    <body>
        
        <!-- Wrapper start -->
        <div id="wrapper">
        
        <!-- Header -->    
        <h1>Hangman</h1>
        
        <!-- Canvas for hangman drawing -->
        <canvas id="hangmanCanvas" width="600" height="300"></canvas>
        <script src="res/js/canvas.js"></script>
        <br>
        
        <!-- Display found and used letters -->
        <span id="word_found">word...</span><br>
        <span id="used_letters">used...</span><br>
        <span id="remaining_attempts">attempts</span><br>
        <br>
        
        <!-- Display all the letters -->
        <div id="letterWrapper">
        <% // print all the letters
        char[] letters = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        for ( char c : letters)
        {
            %><span class="letters"><%out.print(c);%> </span><%
        }
        %>
        </div>
        <br>
        
        <!-- Restart button -->
        <input type="button" id="buttonRestartGame" value="Restart game"></input>
        
        <!-- Wrapper end -->
        </div>

    </body>
</html>
