<%-- 
    Document   : content
    Created on : Nov 7, 2013, 10:25:59 AM
    Author     : okd
--%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="org.json.simple.JSONObject"%>
<%
String responseName = "result";
String action = request.getParameter("action");
String arg = request.getParameter("arg");
if (arg == null)
    arg = "";
if (action != null)
{
    if (action.equals("test"))
    {
        JSONObject obj=new JSONObject();
        obj.put("result","foo");
        out.print(obj);
        out.flush();
    }
    /*************************************
     * TEST: get a new random word
     */
    else if (action.equals("test_getWord"))
    {
        try {
            dk.okdios.hangman_web.WebApplicationWS1 service = new dk.okdios.hangman_web.WebApplicationWS1();
            dk.okdios.hangman_web.HangmanWEB port = service.getHangmanWEBPort();
            java.lang.String result = port.testGetWord();
            JSONObject obj=new JSONObject();
            obj.put(responseName,result);
            out.print(obj);
            out.flush();
        } catch (Exception ex) {
    	// TODO handle custom exceptions here
            //out.println("exception: " + ex);
        }
    }
    /*************************************
     * Get word with only found letters "visible"
     */
    else if (action.equals("getFoundAsString"))
    {
        try {
            dk.okdios.hangman_web.WebApplicationWS1 service = new dk.okdios.hangman_web.WebApplicationWS1();
            dk.okdios.hangman_web.HangmanWEB port = service.getHangmanWEBPort();
            java.lang.String result = port.getFoundAsString();
            JSONObject obj=new JSONObject();
            obj.put(responseName,result);
            out.print(obj);
            out.flush();
        } catch (Exception ex) {
    	// TODO handle custom exceptions here
            //out.println("exception: " + ex);
        }
    }
    /*************************************
     * Get already used letters (which are not in the word)
     */
    else if (action.equals("getUsedLettersAsString"))
    {
        try {
            dk.okdios.hangman_web.WebApplicationWS1 service = new dk.okdios.hangman_web.WebApplicationWS1();
            dk.okdios.hangman_web.HangmanWEB port = service.getHangmanWEBPort();
            java.lang.String result = port.getUsedLettersAsString();
            JSONObject obj=new JSONObject();
            obj.put(responseName,result);
            out.print(obj);
            out.flush();
        } catch (Exception ex) {
    	// TODO handle custom exceptions here
            //out.println("exception: " + ex);
        }
    }
    /*************************************
     * Get already used letters (that are not in the word)
     */
    else if (action.equals("getRemainingAttemptsAsString"))
    {
        try {
            dk.okdios.hangman_web.WebApplicationWS1 service = new dk.okdios.hangman_web.WebApplicationWS1();
            dk.okdios.hangman_web.HangmanWEB port = service.getHangmanWEBPort();
            java.lang.String result = port.getRemainingAttemptsAsString();
            JSONObject obj=new JSONObject();
            obj.put(responseName,result);
            out.print(obj);
            out.flush();
        } catch (Exception ex) {
    	// TODO handle custom exceptions here
            //out.println("exception: " + ex);
        }
    }
    /*************************************
     * Check a letter
     */
    else if (action.equals("checkLetter"))
    {
        try {
            dk.okdios.hangman_web.WebApplicationWS1 service = new dk.okdios.hangman_web.WebApplicationWS1();
            dk.okdios.hangman_web.HangmanWEB port = service.getHangmanWEBPort();
            java.lang.Boolean result = port.checkLetter(arg);
            JSONObject obj=new JSONObject();
            obj.put(responseName,result);
            out.print(obj);
            out.flush();
        } catch (Exception ex) {
    	// TODO handle custom exceptions here
            //out.println("exception: " + ex);
        }
    }
    /*************************************
     * Get remaining attempts
     */
    else if (action.equals("getRemainingAttempts"))
    {
        try {
            dk.okdios.hangman_web.WebApplicationWS1 service = new dk.okdios.hangman_web.WebApplicationWS1();
            dk.okdios.hangman_web.HangmanWEB port = service.getHangmanWEBPort();
            java.lang.Integer result = port.getRemainingAttempts();
            JSONObject obj=new JSONObject();
            obj.put(responseName,result);
            out.print(obj);
            out.flush();
        } catch (Exception ex) {
    	// TODO handle custom exceptions here
            //out.println("exception: " + ex);
        }
    }
    /*************************************
     * Restart game
     */
    else if (action.equals("restartGame"))
    {
        try {
            dk.okdios.hangman_web.WebApplicationWS1 service = new dk.okdios.hangman_web.WebApplicationWS1();
            dk.okdios.hangman_web.HangmanWEB port = service.getHangmanWEBPort();
            port.restartGame();
            JSONObject obj=new JSONObject();
            obj.put(responseName,"restarted");
            out.print(obj);
            out.flush();
        } catch (Exception ex) {
    	// TODO handle custom exceptions here
            //out.println("exception: " + ex);
        }
    }
    /*************************************
     * Check if the game is over
     */
    else if (action.equals("isGameOver"))
    {
        try {
            dk.okdios.hangman_web.WebApplicationWS1 service = new dk.okdios.hangman_web.WebApplicationWS1();
            dk.okdios.hangman_web.HangmanWEB port = service.getHangmanWEBPort();
            java.lang.Boolean result = port.isGameOver();
            JSONObject obj=new JSONObject();
            obj.put(responseName,result);
            out.print(obj);
            out.flush();
        } catch (Exception ex) {
    	// TODO handle custom exceptions here
            //out.println("exception: " + ex);
        }
    }
    /*************************************
     * Check if the user has won
     */
    else if (action.equals("won"))
    {
        try {
            dk.okdios.hangman_web.WebApplicationWS1 service = new dk.okdios.hangman_web.WebApplicationWS1();
            dk.okdios.hangman_web.HangmanWEB port = service.getHangmanWEBPort();
            java.lang.Boolean result = port.won();
            JSONObject obj=new JSONObject();
            obj.put(responseName,result);
            out.print(obj);
            out.flush();
        } catch (Exception ex) {
    	// TODO handle custom exceptions here
            //out.println("exception: " + ex);
        }
    }
}
else
{
    JSONObject obj=new JSONObject();
    obj.put("result","no_data");
    out.print(obj);
    out.flush();
}

%>
