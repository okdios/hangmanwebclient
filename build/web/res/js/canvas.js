var canvas = document.getElementById('hangmanCanvas');
var context = canvas.getContext('2d');
// do stuff here

lineWidthPile   = 8;
lineWidthMan    = 3;
colorGrass      = "#33FF33";
colorWood       = "#A5492A";
colorRope       = "#A5872A";
colorMan        = "#FFFFFF"
context.lineCap = 'round';

function updateCanvas(attemptsLeft, gameOver)
{
    console.log("UPDATING CANVAS: " + attemptsLeft)
    if (attemptsLeft === 10)
        clearCanvas();
    if (attemptsLeft <= 9)
        attemptsLeft9();
    if (attemptsLeft <= 8)
        attemptsLeft8();
    if (attemptsLeft <= 7)
        attemptsLeft7();
    if (attemptsLeft <= 6)
        attemptsLeft6();
    if (attemptsLeft <= 5)
        attemptsLeft5();
    if (attemptsLeft <= 4)
        attemptsLeft4();
    if (attemptsLeft <= 3)
        attemptsLeft3();
    if (attemptsLeft <= 2)
        attemptsLeft2();
    if (attemptsLeft <= 1)
        attemptsLeft1();
    if (attemptsLeft <= 0)
        attemptsLeft0();
    
    // gameOver == 2 means the user has WON he game
    // Only draw if user has less than 6 attempts left, 
    // because that means the head has been drawn
    if (attemptsLeft <= 5 && gameOver === 2)
        happyFace();
    
    // gameOver == 1 means game is over, and user has LOST the game
    else if (gameOver === 1)
        sadFace();
}

function clearCanvas()
{   // clear the canvas
    context.clearRect(0, 0, canvas.width, canvas.height);
}

function attemptsLeft9()
{   // Ground
    context.lineWidth = lineWidthPile;
    context.strokeStyle = colorGrass;   
    context.beginPath();
    context.moveTo(100, 290);
    context.lineTo(500, 290);
    context.stroke();
}

function attemptsLeft8()
{   // Pile
    context.lineWidth = lineWidthPile;
    context.strokeStyle = colorWood;
    context.beginPath();
    context.moveTo(250, 290);
    context.lineTo(250, 50);
    context.stroke();
}

function attemptsLeft7()
{   // Top of pile
    context.lineWidth = lineWidthPile;
    context.strokeStyle = colorWood;
    context.beginPath();
    context.moveTo(250, 50);
    context.lineTo(350, 50);
    context.stroke();
}

function attemptsLeft6()
{   // Rope
    context.lineWidth = lineWidthMan;
    context.strokeStyle = colorRope;
    context.beginPath();
    context.moveTo(350, 50);
    context.lineTo(350, 80);
    context.stroke();
}

function attemptsLeft5()
{ // Head
    context.lineWidth = lineWidthMan;
    context.strokeStyle = colorMan;
    context.beginPath();
    context.arc(350, 95, 15, 0, 360, false);
    context.stroke();
}

function attemptsLeft4()
{   // Body
    context.lineWidth = lineWidthMan;
    context.strokeStyle = colorMan;
    context.beginPath();
    context.moveTo(350, 110);
    context.lineTo(350, 190);
    context.stroke();
}

function attemptsLeft3()
{   // Left arm
    context.lineWidth = lineWidthMan;
    context.strokeStyle = colorMan;
    context.beginPath();
    context.moveTo(350, 120);
    context.lineTo(315, 140);
    context.stroke();
}

function attemptsLeft2()
{   // Right arm
    context.lineWidth = lineWidthMan;
    context.strokeStyle = colorMan;
    context.beginPath();
    context.moveTo(350, 120);
    context.lineTo(385, 140);
    context.stroke();
}

function attemptsLeft1()
{   // Left leg
    context.lineWidth = lineWidthMan;
    context.strokeStyle = colorMan;
    context.beginPath();
    context.moveTo(350, 190);
    context.lineTo(330, 250);
    context.stroke();
}

function attemptsLeft0()
{   // Right leg
    context.lineWidth = lineWidthMan;
    context.strokeStyle = colorMan;
    context.beginPath();
    context.moveTo(350, 190);
    context.lineTo(370, 250);
    context.stroke();
    
    context.beginPath();
    context.arc(350, 105, 5, 1.1*Math.PI, 1.9*Math.PI, false);
    context.stroke();
}

function sadFace()
{
    context.strokeStyle = colorMan;
    drawEyes();
    
    context.beginPath();
    context.arc(350, 105, 6, 1.1*Math.PI, 1.9*Math.PI, false);
    context.stroke();
    
    writeGameOverStatus("You lost!", "red");
}

function happyFace()
{
    context.strokeStyle = colorMan;
    drawEyes();

    context.beginPath();
    context.arc(350, 100, 6, 2.1*Math.PI, 2.9*Math.PI, false);
    context.stroke();
    
    writeGameOverStatus("You won!!", "green");
}

function drawEyes()
{
    // Left eye
    context.lineWidth = 2;
    context.beginPath();
    context.arc(345, 92, 2, 1.1*Math.PI, 3.1*Math.PI, false);
    context.stroke();
    // Right eye
    context.lineWidth = 2;
    context.beginPath();
    context.arc(355, 92, 2, 1.1*Math.PI, 3.1*Math.PI, false);
    context.stroke();
}


function writeGameOverStatus(text, color)
{
    context.textAlign = 'left';
    context.textBaseline = 'bottom';
    context.font = '50pt Calibri';
    context.fillStyle = color;
    context.fillText(text, 85, 295);
}