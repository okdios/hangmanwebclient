
var isGameOver = false;

$(document).ready(function() {
    updateGUI();
    
    $("#buttonRestartGame").click(function(){
        console.log("================== NEW GAME ==================");
        isGameOver = false;
        ajaxCall("restartGame", "", false);
        updateGUI();
    });
    $(".letters").click(function(){
        var selectedLetter = this.innerHTML;
        console.log("selected: " + selectedLetter);
        if (!isGameOver)
        {
            ajaxCall("checkLetter", selectedLetter, false);
            updateGUI();            
        }
        else
        {
            console.log("Not checking letter. Game is over.");
        }
        
    });
});

function updateGUI()
{
    var gameOver = checkGameOver();
    if (gameOver > 0)
        isGameOver = true;
    // update word (display what letters are found)
    var foundWord = ajaxCall("getFoundAsString", null, false);
    $("#word_found").html(foundWord);
    // update list of used letters (not in word)
    var usedLetters = ajaxCall("getUsedLettersAsString", null, false);
    $("#used_letters").html(usedLetters);
    // update remaining attempts
    var remainingAttempts = ajaxCall("getRemainingAttempts", null, false);
    $("#remaining_attempts").html("Attempts left: " + remainingAttempts);
    
    updateCanvas(remainingAttempts, gameOver);
}


// check if the the game is over, and whether the user has won or not
//  - Game NOT over: returns 0
//  - Game over: returns 1
//  - Game over, and user WON: returns 2
function checkGameOver()
{
    // check if the game is over
    var gameOver = ajaxCall("isGameOver", null, false);
    console.log("game over: " + gameOver);
    if (gameOver)
    {
        // check if user has won
        var won = ajaxCall("won", null, false);
        if (won)
        {
            // User has won!
            console.log("User WON!");
            return 2;
        }
        else
        {
            // User lost the game
            console.log("User LOST!");
            return 1
        }
    }
    return 0;
}

function ajaxCall(action, arg, async)
{
    if (arg !== null && arg !== "")
        arg = "&arg=" + arg;
    else
        arg = "";
    console.log("in ajaxCall: " + action);
    var result;
    $.ajax({
            url: "actions.jsp",
            type: "GET",
            data: "action=" + action + arg,
            contentType:"application/json; charset=utf-8",
            dataType:"json",
            async: async,
            statusCode: {
                200: function () {
                    //if (304 == jqxhr.status)
                    //    alert("not modified"); // does not fire
                }
            },
            error: function (responseData) {
                console.error("Error: " + jQuery.parseXML(responseData) + " >>> " + responseData)
            },
        }).done(function (data) {
            result = data["result"];
            console.log("result: " + result);
        });
    return result;
}
